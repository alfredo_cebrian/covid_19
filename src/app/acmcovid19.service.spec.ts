import { TestBed } from '@angular/core/testing';

import { ACMCovid19Service } from './acmcovid19.service';

describe('ACMCovid19Service', () => {
  let service: ACMCovid19Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ACMCovid19Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
